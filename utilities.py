from __future__ import print_function
from __future__ import division

import os
import math
import random
import hashlib

import numpy as np
import pandas as pd

from scipy.misc import imsave
from sklearn.utils import shuffle as sklearn_shuffle

from keras.initializations import normal, get_fans

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def mkdirp(path):
    try:
        os.makedirs(path)
    except OSError:
        pass

def save_images(images, size, path):
    h, w = images.shape[1], images.shape[2]
    img = np.zeros((h * size[0], w * size[1], 3))

    for idx, image in enumerate(images):
        i = idx % size[1]
        j = idx // size[1]
        img[j*h:j*h+h, i*w:i*w+w, :] = image

    return imsave(path, img)

def inverse_transform(images):
    return (images + 1.) / 2.

def batch_iterator(X, y, batch_size=None, shuffle=False):
    num_samples = X.shape[0]

    if batch_size is None:
        batch_size = num_samples

    if shuffle:
        X, y = sklearn_shuffle(X, y)

    for batch_start in range(0, num_samples, batch_size):
        batch_end = min(batch_start + batch_size, num_samples)
        if batch_end - batch_start == 0:
            continue
        X_batch = X[batch_start:batch_end]
        y_batch = y[batch_start:batch_end]
        yield (X_batch, y_batch)
