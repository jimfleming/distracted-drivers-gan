from keras.layers import Input, merge
from keras.layers.core import Dense, Activation, Flatten, Dropout, Reshape
from keras.layers.convolutional import Convolution2D, MaxPooling2D, AveragePooling2D
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import LeakyReLU
from keras.models import Model, Sequential
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard

from layers import UpSampling2D

def generator(gf_dim, input_shape):
    inputs = Input(shape=input_shape)

    x = Dense(gf_dim * 3 * 4)(inputs)
    x = BatchNormalization(axis=-1)(x)
    x = Activation('relu')(x)

    x = Reshape(target_shape=(3, 4, gf_dim))(x)

    x = UpSampling2D(size=(2, 2), dim_ordering='tf')(x)
    x = Convolution2D(gf_dim // 2, 3, 3, border_mode='same', dim_ordering='tf')(x)
    x = Activation('relu')(x)

    x = UpSampling2D(size=(2, 2), dim_ordering='tf')(x)
    x = Convolution2D(gf_dim // 4, 3, 3, border_mode='same', dim_ordering='tf')(x)
    x = Activation('relu')(x)

    x = UpSampling2D(size=(2, 2), dim_ordering='tf')(x)
    x = Convolution2D(gf_dim // 8, 3, 3, border_mode='same', dim_ordering='tf')(x)
    x = Activation('relu')(x)

    x = UpSampling2D(size=(2, 2), dim_ordering='tf')(x)
    x = Convolution2D(3, 3, 3, border_mode='same', dim_ordering='tf')(x)
    x = Activation('tanh')(x)

    return Model(input=inputs, output=x)

def discriminator(df_dim, input_shape):
    inputs = Input(shape=input_shape)

    x = Convolution2D(df_dim * 1, 3, 3, subsample=(2, 2), border_mode='same', dim_ordering='tf')(inputs)
    x = BatchNormalization(axis=-1)(x)
    x = LeakyReLU(0.2)(x)

    x = Convolution2D(df_dim * 2, 3, 3, subsample=(2, 2), border_mode='same', dim_ordering='tf')(x)
    x = BatchNormalization(axis=-1)(x)
    x = LeakyReLU(0.2)(x)

    x = Convolution2D(df_dim * 4, 3, 3, subsample=(2, 2), border_mode='same', dim_ordering='tf')(x)
    x = BatchNormalization(axis=-1)(x)
    x = LeakyReLU(0.2)(x)

    x = Convolution2D(df_dim * 8, 3, 3, subsample=(2, 2), border_mode='same', dim_ordering='tf')(x)
    x = BatchNormalization(axis=-1)(x)
    x = LeakyReLU(0.2)(x)

    x = Flatten()(x)

    x = Dense(1)(x)
    x = Activation('sigmoid')(x)

    return Model(input=inputs, output=x)
