from __future__ import print_function
from __future__ import division

import os
import math
import time
import pickle
import numpy as np

from keras.layers import Input, merge
from keras.layers.core import Dense, Activation, Flatten, Dropout, Reshape
from keras.layers.convolutional import Convolution2D, MaxPooling2D, AveragePooling2D
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import LeakyReLU
from keras.models import Model, Sequential
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard

from sklearn.cross_validation import train_test_split

from model import discriminator, generator
from utilities import mkdirp, md5, batch_iterator, save_images, inverse_transform

gf_dim = 1024
df_dim = 128
z_dim = 100

DATASET_PATH = os.environ.get('DATASET_PATH', 'dataset/data_10_tf.pkl')
print('md5', DATASET_PATH, md5(DATASET_PATH))

CHECKPOINT_PATH = os.environ.get('CHECKPOINT_PATH', 'checkpoints/')
SUMMARY_PATH = os.environ.get('SUMMARY_PATH', 'summaries/')
MODEL_PATH = os.environ.get('MODEL_PATH', 'models/')

mkdirp(CHECKPOINT_PATH)
mkdirp(SUMMARY_PATH)
mkdirp(MODEL_PATH)

NB_EPOCHS = 3
DOWNSAMPLE = 10

WIDTH, HEIGHT, NB_CHANNELS = 640 // DOWNSAMPLE, 480 // DOWNSAMPLE, 3
BATCH_SIZE = 64
SAMPLE_SIZE = 64

with open(DATASET_PATH, 'rb') as f:
    X_train_raw, y_train, X_test, X_test_ids, _ = pickle.load(f)

X_train_raw = X_train_raw.astype(np.float32)
X_test = X_test.astype(np.float32)

mean_train = X_train_raw.mean()
std_train = X_train_raw.std()

mean_test = X_test.mean()
std_test = X_test.std()

X_train_raw = X_train_raw / 255.0
X_test = X_test / 255.0

print('train', 'mean', X_train_raw.mean(), 'std', X_train_raw.std())
print('test', 'mean', X_test.mean(), 'std', X_test.std())

X_train, X_valid, y_train, y_valid = train_test_split(X_train_raw, y_train, test_size=SAMPLE_SIZE, random_state=41)

D_input = Input(shape=(HEIGHT, WIDTH, NB_CHANNELS))
D = discriminator(df_dim, input_shape=(HEIGHT, WIDTH, NB_CHANNELS))(D_input)
D = Model(D_input, D)
D.summary()

G_input = Input(shape=(z_dim,))
G = generator(gf_dim, input_shape=(z_dim,))(G_input)
G = Model(G_input, G)
G.summary()

D__input = Input(shape=(z_dim,))
G_ = generator(gf_dim, input_shape=(z_dim,))(D__input)
D_ = discriminator(df_dim, input_shape=(HEIGHT, WIDTH, NB_CHANNELS))(G_)
D_ = Model(D__input, D_)
D_.summary()

adam = Adam(lr=0.0002, beta_1=0.5, beta_2=0.999, epsilon=1e-08)

G.compile(optimizer=adam, loss='binary_crossentropy')

D.trainable = False
D_.compile(optimizer=adam, loss='binary_crossentropy')

D.trainable = True
D.compile(optimizer=adam, loss='binary_crossentropy')

z_sample = np.random.uniform(-1.0, 1.0, size=(SAMPLE_SIZE , z_dim))
d_overpowered = False

generator_path = os.path.join(MODEL_PATH, 'generator.json')
with open(generator_path, 'w') as f:
    f.write(G.to_json())

discriminator_path = os.path.join(MODEL_PATH, 'discriminator.json')
with open(discriminator_path, 'w') as f:
    f.write(D.to_json())

checkpoint_interval = (X_train.shape[0] // BATCH_SIZE) // 10
step = 0

for epoch in range(NB_EPOCHS):
    print('Epoch {}/{}...'.format(epoch + 1, NB_EPOCHS))

    nb_batches = int(math.ceil(X_train.shape[0] / BATCH_SIZE))
    for batch, (X_batch, y_batch) in enumerate(batch_iterator(X_train, y_train, batch_size=BATCH_SIZE, shuffle=True)):
        print('Batch {}/{}...'.format(batch + 1, nb_batches))

        batch_size = X_batch.shape[0]
        z_batch = np.random.uniform(-1.0, 1.0, size=(batch_size, z_dim))

        print('Generating images...')
        X_ = G.predict_on_batch(z_batch)

        if step == checkpoint_interval:
            print('Saving training images...')
            train_path = os.path.join(SUMMARY_PATH, 'train_{}_{}.jpg'.format(epoch, step))
            save_images(inverse_transform(X_), [8, 8], train_path)

        if not d_overpowered:
            X = np.concatenate((X_batch, X_))
            y = [1] * X_batch.shape[0] + [0] * X_.shape[0]

            print('Training discriminator...')
            loss_d = D.train_on_batch(X, y)
        else:
            print('Evaluating discriminator...')
            loss_d = D.evaluate(X, y, batch_size=batch_size)

        print('Training generator...')
        loss_g = D_.train_on_batch(z_batch, [1] * z_batch.shape[0])

        d_overpowered = loss_d < loss_g / 2

        print('D: {}, G: {}, sum: {}'.format(loss_d, loss_g, loss_d + loss_g))

        if step == checkpoint_interval:
            print('Saving samples...')
            samples = G.predict(z_sample)
            sample_path = os.path.join(SUMMARY_PATH, 'sample_{}_{}.jpg'.format(epoch, step))
            save_images(inverse_transform(samples), [8, 8], sample_path)

            D.save_weights(os.path.join(CHECKPOINT_PATH, 'D_{}_{}.h5'.format(epoch, step)), overwrite=True)
            G.save_weights(os.path.join(CHECKPOINT_PATH, 'G_{}_{}.h5'.format(epoch, step)), overwrite=True)

        step += 1

    print('Saving samples...')
    samples = G.predict(z_sample)
    sample_path = os.path.join(SUMMARY_PATH, 'sample_{}_{}.png'.format(epoch, step))
    save_images(inverse_transform(samples), [8, 8], sample_path)

    D.save_weights(os.path.join(CHECKPOINT_PATH, 'D_{}_{}.h5'.format(epoch, step)), overwrite=True)
    G.save_weights(os.path.join(CHECKPOINT_PATH, 'G_{}_{}.h5'.format(epoch, step)), overwrite=True)
